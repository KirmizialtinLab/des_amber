; This file (wwh_nn_fix.itp) is compatible with any AMBER ff99 variants in gromacs format. 
; Including Tan/Shaw's new parameters and Steinbrecher's phosphate parameters (the later is intrinsically included in Tan/Shaw's)
; Make sure that wwh_nn_fix.itp is optimized with tip4p-4d.itp (included) to repeat Tan/Shaw's findings
; Adapted by Wei-Wei He and Nawavi Naleem, Kirmizialtin Group, Sep 29 2021.

[ atomtypes ]
; phosphate oxygens (O1P/O2P/O3'/O5') are modified, Steinbrecher et al.
; (Follow Steinbrecher et al's paper for consistency)
; nitrogens are devided into 2 categories, Aromatic and Non-aromatic, Tan/Shaw et al.
; Aromatic: [G](NB>>N7 / N*>>N9), [A](NC>>N1,N3 / NB>>N7 / N*>>N9) 
; Non-aromatic: [G](NA>>N1 / N2>>N2 / NC>>N3), [C](N*>>N1 / NC>>N3 / N2>>N4), [A](N2>>N6), [U](N*>>N1 / NA>>N3) 
CA           6      12.01    0.0000  A   3.30040e-01  2.66102e-01 ; Modified from Tan/Shaw ; Weiwei
CN           6      12.01    0.0000  A   3.28500e-01  2.25099e-01 ; Modified from Tan/Shaw ; Weiwei
H            1      1.008    0.0000  A   0.00000e+00  0.00000e+00 ; Modified from Tan/Shaw ; Weiwei
O2           8      16.00    0.0000  A   3.11690e-01  8.78640e-01 ; 'OP' in Steinbrecher paper for Tan/Shaw's PNAS. Here for deprotonated O1P     ; Weiwei
OH           8      16.00    0.0000  A   3.22345e-01  8.80314e-01 ; 'OQ' in Steinbrecher paper for Tan/Shaw's PNAS. Here for protonated O2P       ; Weiwei
OS           8      16.00    0.0000  A   3.15699e-01  7.11280e-01 ; 'OR' in Steinbrecher paper for Tan/Shaw's PNAS. Here for bridging O3' or O5'  ; Weiwei
;NA           7      14.01    0.0000  A   3.28900e-01  7.11280e-01 ; Aromatic nitrogen Modified from Tan/Shaw 
;NN           7      14.01    0.0000  A   3.35070e-01  7.11280e-01 ; Non-aromatic nitrogen Modified from Tan/Shaw
N2N          7      14.01    0.0000  A   3.35070e-01  7.11280e-01 ; Non-aromatic N2 (C,G,A)  
NAN          7      14.01    0.0000  A   3.35070e-01  7.11280e-01 ; Non-aromatic NA (U,G)
N*N          7      14.01    0.0000  A   3.35070e-01  7.11280e-01 ; Non-aromatic N* (C,U)
NCN          7      14.01    0.0000  A   3.35070e-01  7.11280e-01 ; Non-aromatic NC (C,G)
NBA          7      14.01    0.0000  A   3.28900e-01  7.11280e-01 ; Aromatic NB (A,G)
N*A          7      14.01    0.0000  A   3.28900e-01  7.11280e-01 ; Aromatic N* (A,G)
NCA          7      14.01    0.0000  A   3.28900e-01  7.11280e-01 ; Aromatic NC (A)

; Note: the charge of specified atoms of AUGC were modified following Tan/Shaw. Pls check rna.rtp and the paper for details

; summarize the added angles in ffbonded.itp (no NA/NN correction)
;[ dihedraltypes ]
;i   j   k   l     func
; OS  CT  N*  CS    9      -12.00    -0.690920     4  ; Chi for U
; OS  CT  N*  CS    9       38.13    -2.091380     3
; OS  CT  N*  CS    9      -13.58     5.428806     2
; OS  CT  N*  CS    9      147.46     2.540227     1  ; Nawavi, Weiwei
; OS  CT  N*  CP    9      -31.87     0.251408     4  ; Chi for G
; OS  CT  N*  CP    9       27.76    -1.780513     3
; OS  CT  N*  CP    9      -28.03     5.766242     2
; OS  CT  N*  CP    9      -72.86    -3.816912     1  ; Nawavi, Weiwei
; OS  CT  N*  C5    9       40.99     0.270914     4  ; Chi for A
; OS  CT  N*  C5    9       50.52    -1.729732     3
; OS  CT  N*  C5    9      -31.44     4.585726     2  
; OS  CT  N*  C5    9       98.29     3.143585     1  ; Nawavi, Weiwei
; OS  CT  N*  C4    9       -4.92    -1.297730     4  ; Chi for C
; OS  CT  N*  C4    9       28.20    -3.734998     3
; OS  CT  N*  C4    9      -19.77     5.000022     2
; OS  CT  N*  C4    9      111.18     2.839710     1  ; Nawavi, Weiwei
; OS  CI  CT  CT    9         0.0      0.00000     4  ; Gamma for AUGC
; OS  CI  CT  CT    9      -17.79     3.267687     3
; OS  CI  CT  CT    9      -85.90     5.138152     2
; OS  CI  CT  CT    9      -60.31    -1.506018     1
; OH  CI  CT  CT    9         0.0      0.00000     4
; OH  CI  CT  CT    9      -17.79     3.267687     3
; OH  CI  CT  CT    9      -85.90     5.138152     2
; OH  CI  CT  CT    9      -60.31    -1.506018     1  ; Nawavi, Weiwei
; CT  OS  P   OS    9         0.0      0.00000     4  ; Zeta for AUGC
; CT  OS  P   OS    9       29.47     0.787843     3
; CT  OS  P   OS    9        2.98     4.460336     2
; CT  OS  P   OS    9      -26.02    -1.159884     1  ; Nawavi, Weiwei
 
