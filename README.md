# DES_AMBER
The gromacs compatible force field parameters were adopted from Tan et al., PNAS 2018. We used these parameters in HB CUFIX publication.
These parameters were originally developed by Tan. et al for RNA structures. 

If you have any questions or need further information, feel free to contact Kirmizialtin Lab.

https://www.kirmizialtinlab.org

The user's advised to check and validate.  

## Installation

copy the Des_AMBER.ff  folder to the working directory.

```bash
cp  -r  Des_AMBER.ff         \Your\work\directory
```

## Usage

Select the DES_AMBER force field:

```bash
gmx pdb2gmx -f RNA_struc.pdb


Select the Force Field:
From current directory:
 1: DES_AMBER: gromacs compatible version used in HB_CUFIX paper, (adopted from Tan et al., PNAS 2018)
From '/share........./gromacs/top':
 2: AMBER03 protein, nucleic AMBER94 (Duan et al., J. Comp. Chem. 24, 1999-2012, 2003)
 3: AMBER94 force field (Cornell et al., JACS 117, 5179-5197, 1995)
